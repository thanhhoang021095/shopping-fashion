import { createStore, combineReducers, applyMiddleware } from "redux"
import loginReducer from "./components/Login/Login.reducer"
import registerReducer from "./components/Register/Register.reducer"
import productListReducer from "./components/ProductList/productList.reducer"
import productDetailReducer from "./components/ProductDetail/productDetail.reducer"
import addToCartReducer from "./components/Cart/Cart.reducer"
import { compose } from "redux"
import thunk from "redux-thunk"

const logger = store => next => action => {
    // console.log('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    return result;
}
const crashReporter = store => next => action => {
    try {
        return next(action)
    } catch (err) {
        console.error('Caught an exception!', err)
        throw err
    }
}

const reducers = combineReducers(
    {
        loginReducer,
        registerReducer,
        productListReducer,
        productDetailReducer,
        addToCartReducer
    })
export const store = createStore(
    reducers,
    compose(
        applyMiddleware(logger, crashReporter,thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
)