import React, { useContext } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Header from './components/Header/Header.container'
import Footer from './components/Footer/Footer.jsx'
import SideBar from './components/SideBar/SideBar.jsx'
import Sort from './components/Sort/Sort.container'
import Cart from './components/Cart/Cart.container.jsx'
import SearchBy from './components/SearchBy/SearchBy.container.jsx'
import PrivateRoute from './components/PrivateRoute/PrivateRoute.jsx'
import LoginRoute from "./components/Login/LoginRoute.jsx"
import  CheckoutRoute from "./components/Checkout/CheckoutRoute.jsx"
import { ThemeContext } from "./ThemeContext"
// Component Link
import Loading from './components/Loading/Loading'
import Payment from './components/Payment/Payment';

const Layout = React.lazy(() => import('./components/Layout/Layout'))
const Login = React.lazy(() => import('./components/Login/Login.container'))
const NotFound = React.lazy(() => import('./components/NotFound/NotFound'))
const Register = React.lazy(() => import('./components/Register/Register.container'))
const ProductDetail = React.lazy(() => import('./components/ProductDetail/productDetail.container'))
const Checkout = React.lazy(() => import('./components/Checkout/Checkout.container'))
const ProductList = React.lazy(() => import('./components/ProductList/ProductList.container'))

function Main() {
    const { productData, setProductData } = useContext(ThemeContext)

    // const getProduct = (data) => {
    //     setAddProduct([...addProduct, data])
    //     console.log(addProduct);
    // }

    const handleSortEvent = (value) => {
        setProductData(value)
        // console.log(value);
    }

    return (
        <Router>
            <Header>
                <Cart />
            </Header>
            <React.Suspense fallback={<Loading />}>
                <Switch>
                    <Route path="/" exact >
                        <Layout >
                            <ProductList
                                // getProduct={getProduct}
                                // getProductName={getProductName}
                                // productData={productData}
                            />
                            <SideBar>
                                <SearchBy />
                                <Sort
                                    handleSortEvent={handleSortEvent}
                                    productData={productData}
                                />
                            </SideBar>
                        </Layout>
                    </Route>
                    {/* <Route path="/product-detail/:id" component={HomePage}
                        render={(props) => {
                                console.log(props.match.params.id);
                                return (<ProductDetail product_id={props.match.params.id}
                                />)
                            }} >

                    </Route> */}
                    <PrivateRoute path="/product-detail/:id">
                        <ProductDetail />
                    </PrivateRoute>
                    <CheckoutRoute path="/checkout" >
                        <Checkout />
                    </CheckoutRoute>
                    <Route path="/register" >
                        <Register />
                    </Route>
                    <LoginRoute path="/(login|dang-nhap)">
                        <Login />
                    </LoginRoute>
                    <Route  path="/payment">
                        <Payment />
                    </Route>
                    <Route path="*"><NotFound /></Route>
                </Switch>
            </React.Suspense>
            <Footer />
        </Router >
    )
}

export default Main