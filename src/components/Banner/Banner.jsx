import React from 'react';
import { Carousel } from 'antd';
import 'antd/dist/antd.css';
import "./Banner.css";

export default function Banner() {
    return (
        <Carousel autoplay>
            <div>
                <img src="https://www.tiendauroi.com/uploads/default/original/2X/3/39f8561e4894628ae24bc6ca7d28117480f539d0.jpeg"  alt="sendo-banner-1" style={{width:"100%",height:"250px"}} />
            </div>
            <div>
            <img src="https://www.tiendauroi.com/uploads/default/original/2X/7/746dd8a6eb56ef09034a4150b456a311751b8b50.jpeg"  alt="sendo-banner-1" style={{width:"100%",height:"250px"}} />
                
            </div>
           
        </Carousel>
    )
}