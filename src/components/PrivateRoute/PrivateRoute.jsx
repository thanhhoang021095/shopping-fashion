import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import firebase from "firebase"
// import { useState } from 'react';

export default function PrivateRoute(
    {
        children,
        ...restProps
    }) {
    // const [isLogged,setIsLogged] = useState(true)
    const renderChildren = firebase.auth().currentUser ? children :<Redirect to="/login" />
   
    return (
        <Route {...restProps}>
            {renderChildren}
        </Route>
    )
}