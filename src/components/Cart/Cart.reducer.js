import { ADD_TO_CART_SUCCESS, ADD_TO_CART_FAIL } from "./Cart.action"
const initialState = {
    data: [],
    load: false,
    fail: null,
}

export default function addToCartReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_CART_SUCCESS:
            return Object.assign({},state,{
                load: false,
                data:action.payload
            })
        case ADD_TO_CART_FAIL:
            return Object.assign({},state,{
                load: false,
                fail: action.error
            })
        default:
            return state;
    }
}