export const ADD_TO_CART_FAIL = "ADD TO ADD_TO_CART FAIL"
export const ADD_TO_CART_SUCCESS = "ADD TO ADD_TO_CART SUCCESS"


export function addToCartSuccessAction(payload) {
    return {
        type: ADD_TO_CART_SUCCESS,
        payload: payload
    }
}

export function addToCartFailAction(error) {
    return {
        type: ADD_TO_CART_FAIL,
        error: error
    }
}

export const addProductToCart = (product) => {
    return  (dispatch) => {
        try {
            dispatch(addToCartSuccessAction(product))
        }
        catch (error) {
            dispatch(addToCartFailAction("CANNOT ADD THIS PRODUCT TO SHOPPING CART", error))
        }
    }
}