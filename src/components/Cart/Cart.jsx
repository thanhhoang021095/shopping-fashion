import React from 'react';
// import { ThemeContext } from '../../ThemeContext';
import { Link } from "react-router-dom";
import formatPrice from "../../utils/format/formatPrice"
import { notification } from "antd"
import 'antd/dist/antd.css'


export default function Cart(props) {
    const { data, addProductToCart } = props;
    var total_price = props.data.reduce((acc, curr) => acc + (curr.total_unit_price), 0);

    // Toastr Notifications
    const deleteActionNoti = () => {
        const args = {
            message: "Deleted",
            style: {
                backgroundColor: "#ff8a80",
                color: "#000000",
                fontSize: "15px",
                fontWeight: 700,
            },
            description:
                `Your product ${data.name} is deleted from cart`,
            duration: 1,
            placement: "bottom-right"
        };
        notification.error(args);
    };

    // Delete Item from Cart
    const deleteItem = (value) => {
        var newCartData = [...data]
        for (let i = 0; i < newCartData.length; i++) {
            if (newCartData[i].name === value) {
                newCartData.splice(i, 1);
            }
        }
        addProductToCart(newCartData);
        deleteActionNoti()
    }

    // Change Product's Quantity in Cart
    const quantityChangeHandle = (value) => {
        console.log('changed', value);

    }
    const decreaseQuantity = (value) => {
        var decreaseArr = [...data]
        for (let i = 0; i < decreaseArr.length; i++) {
            if (decreaseArr[i].name === value) {
                if (decreaseArr[i].quantity < 2) {
                    deleteItem(value);
                    return;
                }
                else {
                    decreaseArr[i].quantity -= 1;
                    decreaseArr[i].total_unit_price = decreaseArr[i].final_price * decreaseArr[i].quantity;
                }
            }

        }
        addProductToCart([...decreaseArr])
    }
    const increaseQuantity = (value) => {
        var increaseArr = [...data]
        for (let i = 0; i < increaseArr.length; i++) {
            if (increaseArr[i].name === value) {
                increaseArr[i].quantity += 1;
                increaseArr[i].total_unit_price = increaseArr[i].final_price * increaseArr[i].quantity;
            }
        }
        addProductToCart([...increaseArr])
    }

    return (
        <ul className="minicart">
            {data && data.map((elm, key) => (
                <li key={key} className="cart-product-item">
                    <div className="cart-img">
                        <Link to={`/product-detail/${elm.product_id}`} >
                            <img src={elm.img_url} alt="" style={{ height: "88px", width: "120px" }} />
                        </Link>
                    </div>
                    <div className="cart-content">
                        <h3 style={{ marginBottom: "10px", lineHeight: "10px" }}>
                            <Link to={`/product-detail/${elm.product_id}`} className="cart-product-name" >{elm.name} </Link>
                        </h3>
                        <div className="quantity-section">
                            <strong className="qty-title">Quantity:</strong>
                            <div className="product-quantity">
                                <div className="cart-plus-minus">
                                    {/* <p id="quantity-input" onChange={quantityChangeHandle(elm.quantity)}>
                                        {elm.quantity}  </p> */}
                                    <button className="cart-qtybutton" onClick={() => decreaseQuantity(elm.name)} >-</button>
                                    <div  id="cart-qty-input" value={elm.quantity} onChange={quantityChangeHandle(elm.quantity)}>{elm.quantity} </div>
                                    <button className="cart-qtybutton" onClick={() => increaseQuantity(elm.name)} >+</button>
                                </div>
                            </div>

                        </div>
                        <div className="cart-price">
                            <span className="cart-final-price" >{formatPrice(elm.total_unit_price)} VND</span>
                        </div>
                    </div>
                    <div className="del-icon">
                        <a href="/#" onClick={() => deleteItem(elm.name)}>
                            <i className="far fa-trash-alt" ></i>
                        </a>
                    </div>
                </li>
            ))}

            <li>
                <div className="total-price">
                    <strong className="f-left cart-total-title" >Total:</strong>
                    <strong className="f-right cart-price-title">
                        {formatPrice(total_price)} VND
                    </strong>
                </div>
            </li>
            <li>
                <div className="checkout-link">
                    <Link to="/checkout" className="red-color">Checkout</Link>
                </div>
            </li>
        </ul>
    )
}