import {connect} from "react-redux"
import Cart from  "./Cart"
import { addProductToCart } from "./Cart.action"

const mapStateToProps = (state) => ({
    data: state.addToCartReducer.data,
    load: state.addToCartReducer.load
})

const mapDispatchToProps = {
    addProductToCart
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart)