import {connect} from "react-redux"
import ProductItem from  "./ProductItem"
import { addProductToCart } from "../Cart/Cart.action"

const mapStateToProps = (state) => ({
    data:state.addToCartReducer.data,
    load:state.addToCartReducer.load
})

const mapDispatchToProps = {
    addProductToCart
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductItem)