import React, { useContext } from 'react';
import firebase from 'firebase';
import { Redirect,Link } from 'react-router-dom';
import { ThemeContext } from '../../ThemeContext';
import { Tooltip, notification } from "antd"
import 'antd/dist/antd.css'

import formatPrice from "../../utils/format/formatPrice"

export default function ProductItem({
    img_url,
    product_id,
    shop_name, name,
    price,
    final_price,
    data,
    addProductToCart
}) {
    const { userInfo, setUserInfo } = useContext(ThemeContext)
    // const [userInfo, setUserInfo] = useState({})
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            setUserInfo(user)
            // console.log(user, "user")
        }
    });
    const userEmail = userInfo && userInfo.email

    const cartData = [...data];

    // Toastr Notifications
    const successActionNoti = () => {
        const args = {
            message: "Added success",
            style: {
                backgroundColor: "#a5d6a7",
                color: "#000000",
                fontSize: "15px",
                fontWeight: 700,
                marginTop: "60px"
            },
            description:
                `Your product ${name} is added to cart`,
            duration: 1,
        };
        notification.success(args);
    };

    const productInfo = { name, price, final_price, img_url, shop_name, product_id };
    const addToCartClick = () => {
        // Check if the product had been already added in shopping cart
        if (userEmail) {
            productInfo.quantity = 0;
            let nameIndex = cartData.findIndex(elm => elm.name === productInfo.name)
            if (nameIndex === -1) {
                productInfo.quantity = 1;
                productInfo.total_unit_price = productInfo.final_price * productInfo.quantity
                addProductToCart([...cartData, productInfo])
                successActionNoti()
            }
            else {
                cartData[nameIndex].quantity += 1;
                cartData[nameIndex].total_unit_price = cartData[nameIndex].final_price * cartData[nameIndex].quantity
                addProductToCart(cartData)
            }
        }
        else {
            console.log("ban can phai dang nhap")
            return(<Redirect to="/login" />)
        }
    }

    const text = <span>{name}</span>;

    return (
        <div className="col-xl-4 col-lg-6 col-md-6">
            <div className="product-wrapper mb-50">
                <div className="product-img mb-25">
                    <a href="/#" >
                        <img src={img_url} alt="" />
                    </a>
                    <div className="product-action text-center">
                        <a href="/#~" className="product-item-shopping-cart"
                            title="Shopping Cart"
                            onClick={addToCartClick}>
                            <i className="fas fa-shopping-cart" />
                        </a>
                        <Link to={`/product-detail/${product_id}`}
                            className="product-item-shopping-cart"
                            title="Quick View">
                            <i className="fas fa-search" />
                        </Link>
                    </div>
                </div>
                <div className="product-content pr-0">
                    <div className="pro-cat mb-10">
                        <a href="/#" >{shop_name} </a>
                    </div>
                    <Tooltip className="product-tooltip-name" placement="bottom" title={text} >
                        <Link to={`/product-detail/${product_id}`}>
                            <h4 className="product-item-name">
                                {name}
                            </h4>
                        </Link>
                    </Tooltip>
                    <div className="product-meta">
                        <div className="pro-price">
                            <span className="product-final-price">{formatPrice(final_price)} VND</span>
                            <strike className="product-price">{formatPrice(price)} VND</strike>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

