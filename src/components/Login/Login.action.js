import firebase from "firebase"

export const LOGIN_REQUEST = "LOGIN REQUEST"
export const LOGIN_FAIL = "LOGIN FAIL"
export const LOGIN_SUCCESS = "LOGIN SUCCESS"


export function loginRequestAction() {
    return {
        type: LOGIN_REQUEST,
    }
}

export function loginSuccessAction(payload) {
    return {
        type: LOGIN_SUCCESS,
        payload: payload
    }
}

export function loginFailAction(error) {
    return {
        type: LOGIN_FAIL,
        error: error
    }
}

export const loginAction = (email, password) => {
    // REQUEST -LOADING
    // call api
    // => SUCCESS - DATA - 
    // => FAIL - ERROR
    return async(dispatch) => {
        dispatch(loginRequestAction())
        try {
            const result = await firebase.auth().signInWithEmailAndPassword(email,password)
            dispatch(loginSuccessAction(result))
        } 
        catch (error) {
            dispatch(loginFailAction(error))
        }
    }
    
 }