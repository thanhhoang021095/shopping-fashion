import React, {useState} from 'react'
import {Link, useHistory} from "react-router-dom"

export default function Login(props) {

    const [email,setEmail] = useState("")
    const [password,setPassWord] = useState("")
    const history = useHistory()
    
    const inputEmail = (event) => {
        setEmail(event.target.value);
    }   

    const inputPassword = (event) => {
        setPassWord(event.target.value);
    }
    
  const onRegister = () => {
    props.history.push('/register')
  }

    const  handleLogin = async(e) => {
        e.preventDefault();
        props.loginAction(email, password)
    }
    return (
        <main>
            <section className="breadcrumb-area" style={{ backgroundImage: "url('./assets/page-title.png')" }}>
                <div className="container">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="breadcrumb-text text-center">
                                <h1>Login</h1>
                                <ul className="breadcrumb-menu">
                                    <li><Link to="/">home</Link></li>
                                    <li><span>Login</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="login-area pt-100 pb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 offset-lg-2">
                            <div className="basic-login">
                                <h3 className="text-center mb-60">Login From Here</h3>
                                <p className="text-danger">{props.error}</p>
                                <form onSubmit={handleLogin}>
                                    <label>Email Address <span>**</span></label>
                                    <input id="name" type="text" placeholder="abc@gmail.com" onChange={inputEmail}/>
                                    <label>Password <span>**</span></label>
                                    <input id="pass" type="password" placeholder="********" onChange={inputPassword} />
                                    <div className="login-action mb-20 fix">
                                        <span className="log-rem f-left">
                                            <input id="remember" type="checkbox" />
                                            <label>Remember me!</label>
                                        </span>
                                        <span className="forgot-login f-right">
                                            <a href="/#" >Lost your password?</a>
                                        </span>
                                    </div>
                                    <button name="login" className="btn theme-btn-2 w-100" type="submit" onClick={handleLogin}>Login Now</button>
                                    <div className="or-divide"><span>or</span></div>
                                    <button className="btn theme-btn w-100" onClick={onRegister}>Register Now</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}