import { connect } from "react-redux"
import Login from "./Login"
import { loginAction } from "./Login.action"

const mapStateToProps = (store) => ({
    load: store.loginReducer.load,
    result: store.loginReducer.result,
    error: store.loginReducer.error
})

const mapDispatchToProps =  {
        loginAction
} 

export default connect(mapStateToProps, mapDispatchToProps)(Login)