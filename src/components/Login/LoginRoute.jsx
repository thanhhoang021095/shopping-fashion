import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { ThemeContext } from '../../ThemeContext';
export default function LoginRoute({  
  children,
  ...restProps
}) {
  const { userInfo } = useContext(ThemeContext)
  const userEmail = userInfo && userInfo.email;


  return (
    <Route {...restProps}>
      {userEmail ? <Redirect to="/" /> : children}
    </Route>
  )
}