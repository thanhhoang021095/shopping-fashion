import axios from "axios"

export const PRODUCT_DETAIL_REQUEST = "PRODUCT DETAIL REQUEST"
export const PRODUCT_DETAIL_FAIL = "PRODUCT DETAIL FAIL"
export const PRODUCT_DETAIL_SUCCESS = "PRODUCT DETAIL SUCCESS"

export function productDetailRequestAction() {
    return {
        type: PRODUCT_DETAIL_REQUEST,
    }
}

export function productDetailSuccessAction(payload) {
    return {
        type: PRODUCT_DETAIL_SUCCESS,
        payload: payload
    }
}

export function productDetailFailAction(error) {
    return {
        type: PRODUCT_DETAIL_FAIL,
        error: error
    }
}

export const getProductDetailById = (id) => {
    return async (dispatch) => {
        dispatch(productDetailRequestAction())
        try {
            const result = await axios({
                method: "GET",
                url: `https://mapi.sendo.vn/mob/product/${id}/detail/`
            })
            console.log(result.data);
            dispatch(productDetailSuccessAction(result.data))
        }
        catch (error) {
            dispatch(productDetailFailAction("NOT FOUND", error))
        }
    }
}