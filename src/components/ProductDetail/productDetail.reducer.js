import { PRODUCT_DETAIL_REQUEST, PRODUCT_DETAIL_SUCCESS, PRODUCT_DETAIL_FAIL } from "../ProductDetail/productDetail.action";

const initialState = {
    data: null,
    load: false,
    fail: null,
}

export default function productDetailReducer(state = initialState, action) {
    switch (action.type) {
        case PRODUCT_DETAIL_REQUEST:
            return Object.assign({},state,{
                load:true
            })
        case PRODUCT_DETAIL_SUCCESS:
            return Object.assign({},state,{
                load: false,
                data: action.payload
            })
        case PRODUCT_DETAIL_FAIL:
            return Object.assign({},state,{
                load: false,
                fail: action.error
            })
        default:
            return state;
    }
}