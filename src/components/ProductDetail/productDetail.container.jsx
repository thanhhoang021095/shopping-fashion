import {connect} from "react-redux"
import ProductDetail from  "./ProductDetail"
import { getProductDetailById } from "./productDetail.action"

const mapStateToProps = (state) => ({
    data: state.productDetailReducer.data,
    load: state.productDetailReducer.load
})

const mapDispatchToProps = {
    getProductDetailById
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail)