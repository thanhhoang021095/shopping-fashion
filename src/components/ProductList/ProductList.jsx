import React, { useContext } from 'react';
import ProductItem from '../ProductItem/ProductItem.container';
import Banner from "../Banner/Banner";
import { Pagination } from 'antd';
import 'antd/dist/antd.css';
import { useEffect } from 'react';
import { ThemeContext } from "../../ThemeContext";
import Category from '../Category/Category.container';

export default function ProducList(props) {
    const { isSearching, currentPage, setCurrentPage, searchInput } = useContext(ThemeContext);
    const changingPage = (number) => {
        setCurrentPage(number);
        if (isSearching) {
            props.searchProductList(searchInput,number);
        }
        else {
            props.getProductList(undefined, number);
        }
    };
    const { data } = props;
    useEffect(() => {
        props.getProductList();
    }, [])

    return (
        <div className="col-xl-9 col-lg-8">
            {/* tab filter */}
            <div className="banner-carousel">
                <Banner />
            </div>
            {data &&
                <div className="category" >
                    <Category />
                </div>
            }
            <div className="row mb-10">
                <div className="col-xl-3 col-lg-3 col-md-6">
                    <div className="product-showing mb-40">
                        <p>Showing {data.length} results</p>
                    </div>
                </div>

            </div>
            {/* tab content */}
            <div className="tab-content" id="myTabContent">
                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div className="row">
                        {data && data.map((elm, key) => (
                            <ProductItem
                                key={key}
                                getProduct={props.getProduct}
                                getProductName={props.getProductName}
                                img_url={elm.img_url}
                                name={elm.name}
                                shop_name={elm.shop_name}
                                price={elm.price}
                                final_price={elm.final_price}
                                product_id={elm.product_id}
                            // {...elm}
                            />))}

                    </div>
                </div>
            </div>
            <div className="pagination">
                <Pagination current={currentPage} onChange={changingPage} total={500} />
            </div>
        </div>

    )
}
