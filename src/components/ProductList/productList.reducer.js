import { PRODUCT_LIST_REQUEST, PRODUCT_LIST_SUCCESS, PRODUCT_LIST_FAIL } from "../ProductList/ProductList.action";

const initialState = {
    data: [],
    load: false,
    fail: null,
}

export default function productListReducer(state = initialState, action) {
    switch (action.type) {
        case PRODUCT_LIST_REQUEST:
            return Object.assign({},state,{
                load:true
            })
        case PRODUCT_LIST_SUCCESS:
            return Object.assign({},state,{
                load: false,
                data: action.payload
                
            })

        case PRODUCT_LIST_FAIL:
            return Object.assign({},state,{
                load: false,
                fail: action.error
            })
        default:
            return state;
    }
}