import { connect } from "react-redux"
import ProductList from "./ProductList"
import {getProductList, searchProductList} from "./ProductList.action";

const mapStateToProps = (state) => ({
    load: state.productListReducer.load,
    data: state.productListReducer.data,
})

const mapDispatchToProps = {
    getProductList,
    searchProductList
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductList)