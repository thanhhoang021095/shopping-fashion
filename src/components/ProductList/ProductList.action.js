// import data from "../../data.json"
import axios from "axios"
import { productDetailFailAction } from "../ProductDetail/productDetail.action"
export const PRODUCT_LIST_REQUEST = "PRODUCT LIST REQUEST"
export const PRODUCT_LIST_FAIL = "PRODUCT LIST FAIL"
export const PRODUCT_LIST_SUCCESS = "PRODUCT LIST SUCCESS"

export function productListRequestAction() {
    return {
        type: PRODUCT_LIST_REQUEST,
    }
}

export function productListSuccessAction(payload) {
    return {
        type: PRODUCT_LIST_SUCCESS,
        payload: payload
    }
}

export function productListFailAction(error) {
    return {
        type:   PRODUCT_LIST_FAIL,
        error: error
    }
}

export const getProductList = (name="ao-so-mi-nam", page = 1) => {
    return async (dispatch) => {
        dispatch(productListRequestAction())
        try {
            const result = await axios({
                method:"GET",
                url: `https://mapi.sendo.vn/mob/product/cat/${name}?p=${page}`
            })
            // console.log(result);
            dispatch(productListSuccessAction(result.data.data))
        } catch (error) {
            dispatch(productDetailFailAction(error))
        }
        
    }
}

export const searchProductList = (productName="ao-so-mi-nam",page=1) => {
    return async (dispatch) => {
        dispatch(productListRequestAction())
        try {
            const result = await axios({
                method:"GET",
                url: `https://mapi.sendo.vn/mob/product/search?p=${page}&q=<${productName}>`
            })
            // console.log(result);
            dispatch(productListSuccessAction(result.data.data))
        } catch (error) {
            dispatch(productDetailFailAction(error))
        }
        
    }
}

export const getProductListBySort = (data) => {
    return async (dispatch) => {
        dispatch(productListSuccessAction(data))
    }
}