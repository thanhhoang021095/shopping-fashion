import React, { useState } from "react"
import { Link } from "react-router-dom";
import { notification } from "antd"
import 'antd/dist/antd.css'

import formatPrice from "../../utils/format/formatPrice"

export default function Checkout(props) {
    const [couponText, setCouponText] = useState("");
    const [appliedCoupon, setAppliedCoupon] = useState(false);
    const { data, addProductToCart } = props;
    var total_price = props.data.reduce((acc, curr) => acc + (curr.total_unit_price), 0);
    // Success Toastr Notification
    const successNoti = () => {
        const args = {
            message: "Success",
            style: {
                fontWeight: 700,
                backgroundColor: "#a5d6a7",
                color: "#000000",
                fontSize: "15px",
                marginTop: "60px"
            },
            description:
                `Your coupon code is applied`,
            duration: 4,
        };
        notification.success(args);
    };
    const invalidCoupon = () => {
        const args = {
            message: "Failed",
            style: {
                backgroundColor: "#ff8a80",
                color: "#000000",
                fontSize: "15px",
                fontWeight: 700,
                marginTop: "60px"
            },
            description:
                `Your coupon code is incorrect`,
            duration: 4,
        };
        notification.error(args);
    };
    const failToReuseCoupon = () => {
        const args = {
            message: "Failed",
            style: {
                backgroundColor: "#ff8a80",
                color: "#000000",
                fontSize: "15px",
                fontWeight: 700,
                marginTop: "60px"
            },
            description:
                `Your coupon is applied. You cannot use any coupon anymore !`,
            duration: 4,
        };
        notification.error(args);
    };


    // Delete Product Function
    const deleteItem = (value) => {
        var newCheckoutData = [...data]
        for (let i = 0; i < newCheckoutData.length; i++) {
            if (newCheckoutData[i].name === value) {
                newCheckoutData.splice(i, 1);
            }
        }
        props.addProductToCart(newCheckoutData);
    }

    // Change Product's Quantity in Cart
    const quantityChangeHandle = (value) => {
        console.log('changed', value);

    }
    const decreaseQuantity = (value) => {
        var decreaseArr = [...data]
        for (let i = 0; i < decreaseArr.length; i++) {
            if (decreaseArr[i].name === value) {
                if (decreaseArr[i].quantity < 2) {
                    deleteItem(value);
                    return;
                }
                else {
                    decreaseArr[i].quantity -= 1;
                    if (couponText === "LOVE SHOPPING") {
                        decreaseArr[i].total_unit_price = decreaseArr[i].final_price * decreaseArr[i].quantity;

                        // decreaseArr[i].total_unit_price = (decreaseArr[i].final_price * decreaseArr[i].quantity) / 2;
                    }
                    decreaseArr[i].total_unit_price = decreaseArr[i].final_price * decreaseArr[i].quantity;
                }
            }

        }
        addProductToCart([...decreaseArr])
    }
    const increaseQuantity = (value) => {
        var increaseArr = [...data]
        for (let i = 0; i < increaseArr.length; i++) {
            if (increaseArr[i].name === value) {
                increaseArr[i].quantity += 1;
                // console.log("coupon",couponText);
                if (couponText === "LOVE SHOPPING") {
                    increaseArr[i].total_unit_price = increaseArr[i].final_price * increaseArr[i].quantity;

                    // increaseArr[i].total_unit_price = (increaseArr[i].final_price * increaseArr[i].quantity) / 2;
                }
                increaseArr[i].total_unit_price = increaseArr[i].final_price * increaseArr[i].quantity;
            }
        }
        addProductToCart([...increaseArr])
    }

    // Input Coupon Funtion
    const inputCoupon = (e) => {
        setCouponText(e.target.value);
    }
    const applyCoupon = (e) => {
        e.preventDefault();
        if (appliedCoupon === false && couponText === "LOVE SHOPPING") {
            var couponArr = [...data];
            for (let i = 0; i < couponArr.length; i++) {
                couponArr[i].total_unit_price /= 2;
                couponArr[i].final_price /= 2;
                // setTotalPrice(totalPrice / 2);
            }
            setCouponText("");
            successNoti();
            setAppliedCoupon(true);
            addProductToCart([...couponArr]);
        }
        else {
            if (couponText === "LOVE SHOPPING") {
                setCouponText("");
                failToReuseCoupon();
            }
            else {
                setCouponText("");
                invalidCoupon();
            }
        }
    };


    return (
        <main>
            <section className="breadcrumb-area" style={{ backgroundImage: 'url("./assets/page-title.png")' }}>
                <div className="container">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="breadcrumb-text text-center">
                                <h1>Shoping Cart</h1>
                                <ul className="breadcrumb-menu">
                                    <li><a href="index.html">home</a></li>
                                    <li><span>Cart</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="cart-area pt-100 pb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-12" >
                            <form onSubmit={applyCoupon}>
                                <div className="table-content table-responsive">
                                    <table className="table">
                                        <thead>
                                            <tr style={{ border: "2px dashed" }}>
                                                <th className="product-thumbnail" style={{ borderRight: "2px dashed" }}>Images</th>
                                                <th className="cart-product-name" style={{ borderRight: "2px dashed" }}>Product</th>
                                                <th className="product-price-title" style={{ borderRight: "2px dashed" }}>Unit Price</th>
                                                <th className="product-quantity" style={{ borderRight: "2px dashed" }}>Quantity</th>
                                                <th className="product-subtotal" style={{ borderRight: "2px dashed" }}>Total</th>
                                                <th className="product-remove">Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {data.map((elm, key) => (
                                                < tr key={key} style={{ border: "2px dashed" }}>
                                                    <td className="product-thumbnail" style={{ borderRight: "2px dashed" }}><Link to={`/product-detail/${elm.product_id}`}><img src={elm.img_url}
                                                        alt="" /></Link></td>
                                                    <td className="product-name" style={{ borderRight: "2px dashed" }}><Link to={`/product-detail/${elm.product_id}`} >{elm.name}</Link></td>
                                                    <td className="product-price" style={{ borderRight: "2px dashed" }}><span className="amount">{formatPrice(elm.final_price)} VND</span></td>
                                                    <td className="product-quantity" style={{ borderRight: "2px dashed" }}>
                                                        <div className="cart-plus-minus">
                                                            <input type="text" value={elm.quantity} style={{ backgroundColor: "#cc2b5e", color: "#fff" }} onChange={quantityChangeHandle(elm.quantity)} />
                                                            <div className="dec qtybutton" style={{ color: "#cc2b5e", fontSize: "26px" }} onClick={() => decreaseQuantity(elm.name)}>
                                                                -
                                                            </div>
                                                            <div className="inc qtybutton" style={{ color: "#cc2b5e", fontSize: "26px" }} onClick={() => increaseQuantity(elm.name)}>
                                                                +
                                                            </div>
                                                        </div>
                                                    </td>
                                                    {appliedCoupon ?
                                                        <td className="product-subtotal" style={{ borderRight: "2px dashed" }}>
                                                            <span className="amount" style={{display:"grid"}}>
                                                                {formatPrice(elm.total_unit_price)} VND
                                                                <span style={{ fontSize: "12px", color: "#cc2b5e", marginRight: "10px" }}>
                                                                    Applying Coupon 50%
                                                                </span>
                                                            </span>
                                                        </td>
                                                        :
                                                        <td className="product-subtotal" style={{ borderRight: "2px dashed" }}>
                                                            <span className="amount" >
                                                                {formatPrice(elm.total_unit_price)} VND
                                                            </span>
                                                        </td>
                                                    }
                                                    <td >
                                                        <i className="fa fa-times product-remove-icon" onClick={() => deleteItem(elm.name)}>
                                                        </i>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="coupon-all">
                                            <div className="coupon">
                                                <input id="coupon_code" className="input-text" name="coupon_code" placeholder="Input your coupon..."
                                                    value={couponText} type="text" onChange={inputCoupon} />
                                                <input type="submit" className="btn theme-btn-2"
                                                    name="apply_coupon" value="Apply coupon"
                                                    style={{ backgroundColor: "#cc2b5e", color: "#fff", fontSize:"14px",height:"45px" }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-5 ml-auto">
                                        <div className="cart-page-total">
                                            <h2>Cart totals</h2>
                                            {appliedCoupon ?
                                                <ul className="mb-20">
                                                    < li style={{ borderBottom: "2px dashed" }}>Subtotal
                                                        <span>
                                                            <span style={{ fontSize: "12px", color: "#cc2b5e", marginRight: "10px" }}>
                                                                Applying Coupon 50%
                                                            </span>{formatPrice(total_price)}
                                                        </span>
                                                    </li>
                                                    <li>Total
                                                        <span>
                                                            <span style={{ fontSize: "12px", color: "#cc2b5e", marginRight: "10px" }}>
                                                                Applying Coupon 50%
                                                            </span>{formatPrice(total_price)}
                                                        </span>
                                                    </li>
                                                </ul>
                                                :
                                                <ul className="mb-20">
                                                    <li style={{ borderBottom: "2px dashed" }}>Subtotal <span>{formatPrice(total_price)}</span></li>
                                                    <li>Total <span>{formatPrice(total_price)}</span></li>
                                                </ul>
                                            }
                                            <Link to="/payment" className="btn theme-btn" >Proceed to payment</Link>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main >
    )
}