import {connect} from "react-redux"
import Checkout from  "./Checkout"
import { addProductToCart } from "../Cart/Cart.action"

const mapStateToProps = (state) => ({
    data: state.addToCartReducer.data,
    load: state.addToCartReducer.load
})

const mapDispatchToProps = {
    addProductToCart
}
export default connect(mapStateToProps, mapDispatchToProps)(Checkout)