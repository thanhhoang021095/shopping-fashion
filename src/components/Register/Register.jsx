import React,{useState} from 'react'
import {Link} from "react-router-dom"
export default function Register(props) {
    const [email,setEmail] = useState("")
    const [password,setPassWord] = useState("")

    const inputEmail = (event) => {
        setEmail(event.target.value);
        
    }

    const inputPassword = (event) => {
        setPassWord(event.target.value);
    }

    const  handleRegister = async(e) => {
        e.preventDefault();
        props.registerAction(email,password)
        // const loadingRegister = () => {
            
        // }
    }
    return (
        <main>
            <section className="breadcrumb-area" style={{ backgroundImage: 'url("./assets/page-title.png")' }}>
                <div className="container">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="breadcrumb-text text-center">
                                <h1>Register</h1>
                                <ul className="breadcrumb-menu">
                                    <li><Link to="/">home</Link></li>
                                    <li><span>Register</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="login-area pt-100 pb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 offset-lg-2">
                        <div className="basic-login">
                                <h3 className="text-center mb-60">Signup From Here</h3>
                                <p className="text-danger">{props.error}</p>
                                <form onSubmit={handleRegister}>
                                    <label htmlFor="email-id">Email Address <span>**</span></label>
                                    <input id="pass" type="email" placeholder="xyz@gmail.com" onChange={inputEmail} />
                                    <label htmlFor="pass">Password <span>**</span></label>
                                    <input id="pass" type="password" placeholder="Enter password..." onChange={inputPassword} />
                                    <div className="mt-10"></div>
                                    <button className="btn theme-btn-2 w-100" type="submit">Register Now</button>
                                    <div className="or-divide"><span>or</span></div>
                                    <button className="btn theme-btn w-100">login Now</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

    )
}