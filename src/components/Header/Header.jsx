import React, { useContext } from 'react';
import firebase from 'firebase';
import { ThemeContext } from '../../ThemeContext';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import 'antd/dist/antd.css'


function Header(props) {
    const { data } = props
    const { userInfo, setUserInfo,setCurrentPage } = useContext(ThemeContext)
    // const [userInfo, setUserInfo] = useState({})
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            setUserInfo(user)
            // console.log(user, "user")
        }
    });

    const onLogout = async () => {
        await firebase.auth().signOut()
        window.location.href = "/"
    }

    const userEmail = userInfo && userInfo.email
    const returnDefault = () => {
        setCurrentPage(1);
        props.getProductList("ao-so-mi-nam",1);
    }
    return (
        <div>
            <header>
                <div id="header-sticky" className="header-area box-90 sticky-header">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-xl-2 col-lg-6 col-md-6 col-7 col-sm-5 d-flex align-items-center pos-relative">
                                <div className="logo" onClick={returnDefault}>
                                    <Link to="/" ><img src="./assets/logo_shop_header.png" style={{ height: "80px", maxWidth: "100%" }} alt="" /></Link>
                                </div>

                                <div className="category-menu">
                                    <h4>Category</h4>
                                    <ul>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Table lamp</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Furniture</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Chair</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Men</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Women</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Cloth</a></li>
                                        <li><a href="/#" ><i className="fas fa-shopping-cart"></i> Trend</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-xl-8 col-lg-6 col-md-8 col-8 d-none d-xl-block">
                                <div className="main-menu text-center">
                                    <nav id="mobile-menu" style={{ display: 'block' }}>
                                        <ul style={{ marginBottom: "0px" }}>
                                            <li>
                                                <Link to="/" className="main-menu-home-title" onClick={returnDefault}>Home</Link>
                                            </li>
                                            <li>
                                                <a href="/#" className="main-menu-page-title">Pages</a>
                                                <ul className="submenu" >
                                                    <li>
                                                        <Link to={`/product-detail/${data.product_id}`}>Product Detail</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="/login">login</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="/register">Register</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="/checkout">Shoping Cart</Link>
                                                    </li>
                                                </ul>
                                            </li>
                                            {
                                                userEmail &&
                                                <li>
                                                    <h6 className="login-welcome" style={{ color: "#cc2b5e" }}>Hello {userInfo.email}</h6>
                                                </li>
                                            }
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                            <div className="col-xl-2 col-lg-6 col-md-6 col-5 col-sm-7 pl-0" style={{ display: "inline-flex" }}>
                                {/* <div className="login-text">
                                    
                                </div> */}

                                <div className="header-right f-right" >

                                    <ul style={{ marginBottom: "0", display: "inline-flex" }}>
                                        {/* <li className="search-btn">
                                            <a href="/#" className="search-btn nav-search search-trigger" ><i className="fas fa-search" ></i></a>
                                        </li> */}
                                        {
                                            userEmail
                                                ? <li className="signout-item"><Button className="signout-btn"><Link to="/#" onClick={onLogout}>Sign Out</Link></Button></li>
                                                :
                                                <React.Fragment>
                                                    <li className="login-item"><Button className="login-btn"><Link to="/login" >Login</Link></Button></li>
                                                    <li className="register-item"><Button className="register-btn"><Link to="/register" >Register</Link></Button></li>
                                                </React.Fragment>
                                        }

                                        {userEmail &&
                                            <li className="d-shop-cart"><a href="/#" className="main-header-shopping-cart"><i className="fas fa-shopping-cart"></i>
                                                <span className="cart-count">{data.length}</span>
                                            </a>
                                                {props.children}
                                            </li>

                                        }
                                    </ul>
                                </div>
                            </div>
                            <div className="col-12 d-xl-none">
                                <div className="mobile-menu"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header></div>
    )
}

export default Header