import { connect } from "react-redux"
import Header from "./Header"
import {addProductToCart} from "../Cart/Cart.action";
import {getProductList} from "../ProductList/ProductList.action"

const mapStateToProps = (state) => ({
    data:state.addToCartReducer.data
})

const mapDispatchToProps = {
    addProductToCart,
    getProductList
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)