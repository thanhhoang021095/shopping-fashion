import {connect} from "react-redux";
import Sort from "./Sort"
import {getProductListBySort} from "../ProductList/ProductList.action"

const mapStateToProps = (state) => ({
    load: state.productListReducer.load,
    data: state.productListReducer.data,
})

const mapDispatchToProps = {
    getProductListBySort
}
export default connect(mapStateToProps,mapDispatchToProps)(Sort)