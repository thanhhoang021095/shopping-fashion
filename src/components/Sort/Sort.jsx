import React from 'react'

export default function Sort(props) {
    const { data, getProductListBySort } = props
    var newProductData = [...data];
    const sortAtoZ = () => {
        const productAtoZ  = newProductData.sort((a, b) => a.name.localeCompare(b.name))
        getProductListBySort(productAtoZ)
    }
    const sortZtoA = () => {
        const productZtoA = newProductData.sort((a, b) => a.name.localeCompare(b.name)).reverse()
        getProductListBySort(productZtoA)
    }
    const sortHightoLow = () => {
        const priceHightoLow = newProductData.sort((a, b) => a.final_price-b.final_price).reverse()
        getProductListBySort(priceHightoLow)
    }
    const sortLowtoHigh = () => {
        const priceLowtoHigh = newProductData.sort((a, b) => a.final_price-b.final_price)
        getProductListBySort(priceLowtoHigh)
    }
    return (
        <ul className="shop-link">
            <li><a href="/#~" onClick={sortAtoZ} >Name: A-Z</a></li>
            <li><a href="/#~" onClick={sortZtoA}>Name: Z-A</a></li>
            <li><a href="/#~" onClick={sortHightoLow}>Price: High to Low</a></li>
            <li><a href="/#~" onClick={sortLowtoHigh}>Price: Low to High</a></li>
            <li><a href="/#~">Product: Top Sales</a></li>
        </ul>
    )
}