import React from 'react';
import { Button } from "antd";
import 'antd/dist/antd.css';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "./Category.css"
import sport from "../../images/category/sport.png";
import motor from "../../images/category/motorbike.png";
import toys from "../../images/category/toys.png";
import phone from "../../images/category/phone.png";
import funiture from "../../images/category/funiture.png";
import stove from "../../images/category/stove.png";
import laptop from "../../images/category/laptop.png";
import watch from "../../images/category/watch.png";
import book from "../../images/category/book.png";
import cloth from "../../images/category/cloth.png";
import { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';

export default function Category(props) {
    const { setIsSearching, setSearchInput } = useContext(ThemeContext)
    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3,
            slidesToSlide: 3, // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
            slidesToSlide: 2, // optional, default to 1.
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
            slidesToSlide: 1, // optional, default to 1.
        },
    };

    const getCategoryItem = (keyword) => {
        setIsSearching(true);
        setSearchInput(keyword);
        props.searchProductList(keyword, 1);
    };
    return (
        <Carousel
            swipeable={false}
            draggable={false}
            showDots={false}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlaySpeed={500}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-20-px"
        >
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("phuong-tien")}>
                    <img
                        src={motor}
                        alt="motor"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Vehihle</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("do-choi")}>
                    <img
                        src={toys}
                        alt="toys"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Toys</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("dien-thoai")}>
                    <img
                        src={phone}
                        alt="phone"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Phone</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("noi-that")}>
                    <img
                        src={funiture}
                        alt="funiture"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Funiture</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("the-thao")}>
                    <img
                        src={sport}
                        alt="sport"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Sport</div> 
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("laptop")}>
                    <img
                        src={laptop}
                        alt="laptop"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Laptop</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("sach")}>
                    <img
                        src={book}
                        alt="book"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Book</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("quan-ao")}>
                    <img
                        src={cloth}
                        alt="cloth"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Clothes</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("bep-ga")}>
                    <img
                        src={stove}
                        alt="stove"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Kitchen</div>
                </Button>
            </div>
            <div className="category-item">
                <Button className="category-item-btn" onClick={() => getCategoryItem("dong-ho")}>
                    <img
                        src={watch}
                        alt="watch"
                        style={{
                            display: 'block',
                            height: '60px',
                            margin: 'auto',
                            width: '100%'
                        }}
                    />
                <div className="item-subtitle">Watch</div>
                </Button>
            </div>
        </Carousel>
    )
}