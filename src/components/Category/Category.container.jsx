import {connect} from "react-redux";
import Category from "./Category";
import {searchProductList} from "../ProductList/ProductList.action";

const mapStateToProps = (state) => ({
    load: state.productListReducer.load,
    data: state.productListReducer.data,
});

const mapDispatchToProps = {
    searchProductList
}

export default connect(mapStateToProps,mapDispatchToProps)(Category)
