import React from 'react'
import { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';

export default function SearchBy(props) {
    const [searchText, setSearchText] = React.useState("");
    const {setIsSearching, setSearchInput} = useContext(ThemeContext);

    const inputText = (event) => {
        setSearchText(event.target.value);
    }
    const handleSubmit = (e) => {
        e.preventDefault()

        // const [textInput] = e.target.elements;
        // const keyword = textInput.value
        // console.log(keyword);
        setIsSearching(true);
        setSearchInput(searchText);
        props.searchProductList(searchText,1);
        setSearchText("");
    }
   
    return (
        <div className="shop-widget">
            <h3 className="shop-title">Search by</h3>
            <form  name="text" onSubmit={handleSubmit} className="shop-search">
                <input  type="text" value={searchText} placeholder="Your keyword...." onChange={inputText} />
                <button><i className="fa fa-search" /></button>
            </form>
        </div>
    )
}