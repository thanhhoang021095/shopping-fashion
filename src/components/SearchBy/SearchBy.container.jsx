import { connect } from "react-redux"
import {searchProductList} from "../ProductList/ProductList.action"
import SearchBy from "./SearchBy"

const mapStateToProps = (state) => ({
    load: state.productListReducer.load,
    data: state.productListReducer.data
})

const mapDispatchToProps = {
    searchProductList
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBy)