import React, { useState, useEffect } from 'react';
import data from './data.json'
import firebase from "firebase"


export const ThemeContext = React.createContext("");
function ThemeContextComponent({ children }) {

    const [user, setUser] = React.useState()

    useEffect(() => {
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          setUser(user)
        }
    });
    })

    //Product
    const [productData, setProductData] = useState(data.data);
    const [addProduct, setAddProduct] = useState([]);
   
    //cart
    const [cartItem, setCartItem] = useState([]);

    //user
    const [userInfo, setUserInfo] = useState({});

    //product detail    
    const [itemDetail, setItemDetail] = useState([]);

    // pagination
    const [currentPage,setCurrentPage] = useState(1);

    // search proccesing
    const [isSearching, setIsSearching] = useState(false);
    
    //switch background-color
    const [themeValue, setThemeValue] = useState("white");
    function switchTheme() {
        if (themeValue === "white") {
            setThemeValue("black")
        }
        else {
            setThemeValue("white")
        }
    }

    // Search Text
    const [searchInput, setSearchInput] = useState("")
    return (
        <ThemeContext.Provider value={{
            switchTheme, value: themeValue,
            addProduct, setAddProduct,
            productData, setProductData,
            cartItem, setCartItem,
            itemDetail, setItemDetail,
            userInfo, setUserInfo,
            currentPage,setCurrentPage,
            isSearching, setIsSearching,
            searchInput, setSearchInput
        }}>
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeContextComponent;